function dashboardCtrl($scope, globalConst, facultySrvc, commonSrvc, $uibModal, toastr, $rootScope){
    var vm = this;
    $rootScope.leftNavBar = true;
    $rootScope.topNavBar = true;
    $rootScope.role = commonSrvc.getRole();
}
var dashboardApp = angular.module('bips.dashboardApp', []);
dashboardApp.controller('dashboardCtrl', dashboardCtrl);
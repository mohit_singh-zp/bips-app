function loginCtrl($scope, $state, loginSrvc, commonSrvc, $rootScope, $sessionStorage){
    var vm = this;
    $rootScope.leftNavBar = false;
    $rootScope.topNavBar = false;
    $sessionStorage.remove('auth');
    $sessionStorage.remove('role');
    vm.userLogin = function(data){
        loginSrvc.login(data).then(function(response){
            $state.go('dashboard');
        });
    };
}
var loginApp = angular.module('bips.loginApp', []);
loginApp.controller('loginCtrl', loginCtrl);

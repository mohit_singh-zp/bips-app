function loginSrvc(commonSrvc){
    var factory = {};
    var header = commonSrvc.getAuth();
    factory.login = function(data){
        return commonSrvc.post('user/api/v1/login', data, header).then(function(response){
            commonSrvc.setAuth(response.data.Authorization, response.data.role);
            return response;
        });
    };
    return factory;
}
loginApp.factory('loginSrvc', loginSrvc);
function studentSrvc(commonSrvc, $sessionStorage){
    var factory = {};
    factory.getStudentList = function(){
        return commonSrvc.get('student/api/v1/get/list', '', $sessionStorage.get('auth')).then(function(response){
            return response;
        });
    };
    return factory;
}
studentApp.factory('studentSrvc', studentSrvc);
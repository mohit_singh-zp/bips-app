function studentCtrl($scope, $state, studentSrvc, commonSrvc, $uibModal, globalConst, toastr, $rootScope){
    var vm = this;
    $rootScope.leftNavBar = true;
    $rootScope.topNavBar = true;
    vm.getStudentList = function(){
        studentSrvc.getStudentList().then(function(response){
            vm.list = response.data.data;
            if(!vm.list.length){
                $state.go('admission');
            }
        });
    };

    vm.openStudentDetails = function(index){
        vm.index = index;
        $uibModal.open({
            templateUrl: '/app/modules/student/partials/view-student.html',
            controller: showStudentDetail,
            scope: $scope,
            size: 'lg'
        });
    };

    function showStudentDetail($scope, $uibModalInstance, globalConst) {
        vm.date = globalConst.date;
        vm.format = globalConst.dateFormat;
        vm.dateOptions = globalConst.datePickerOpt;
        vm.open = function() {
            vm.popup.opened = true;
        };
        vm.popup = {
            opened: false
        };
        vm.bloodGroup = globalConst.bloodGroup;
        vm.motherTongue = globalConst.motherTongue;
        vm.id = globalConst.id;
        vm.idList = globalConst.idList;
        var cityState = globalConst.cityState;
        vm.states = Object.keys(cityState);
        vm.updateCityList = function(state){
            vm.cities = cityState[state];
        };

        vm.details = vm.list[vm.index];
        vm.modalHeader = "Student Details";
        if(vm.details.address.state){
            vm.updateCityList(vm.details.address.state);
        }

        vm.ok = function () {
            $uibModalInstance.close();
        };
        
        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    vm.deleteUser = function(){
        
    };
}
var studentApp = angular.module('bips.studentApp', []);
studentApp.controller('studentCtrl', studentCtrl);

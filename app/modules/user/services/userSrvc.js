function userSrvc(commonSrvc, $sessionStorage){
    var factory = {};
    factory.getUserList = function(){
        return commonSrvc.get('user/api/v1/get/list', '', $sessionStorage.get('auth')).then(function(response){
            return response;
        });
    };

    factory.saveUser = function(data){
        return commonSrvc.post('user/api/v1/register', data, $sessionStorage.get('auth')).then(function(response){
            return response;
        });
    };

    factory.modifyUser = function(data){
        return commonSrvc.update('user/api/v1/update', data, $sessionStorage.get('auth')).then(function(response){
            return response;
        });
    };

    factory.deleteUser = function(data){
        return commonSrvc.update('user/api/v1/delete', data, $sessionStorage.get('auth')).then(function(response){
            return response;
        });
    };
    return factory;
}
userApp.factory('userSrvc', userSrvc);
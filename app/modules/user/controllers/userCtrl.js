function userCtrl($scope, $state, userSrvc, commonSrvc, $uibModal, globalConst, toastr, $rootScope){
    var vm = this;
    $rootScope.leftNavBar = true;
    $rootScope.topNavBar = true;
    vm.cities = [];
    vm.role = globalConst.role;
    vm.active = 0;
    vm.tabs = [
        { id: 0, title:'view', content:'modules/user/user-view.html' },
        { id: 1, title:'create', content:'modules/user/user-create.html' }
    ];
    
    var cityState = globalConst.cityState;
    vm.states = Object.keys(cityState);
    vm.updateCityList = function(state){
        vm.cities = cityState[state];
    };

    vm.manageTab = function(tabIndex){
        if(tabIndex === 1){
            vm.userDetail = {};
        }
    };

    vm.save = function(data){
        vm.btnDisabled = userSrvc.saveUser(data).then(function(response){
            toastr.success('Saved Successfully');
            vm.active = 0;
            vm.userDetail = {};
            vm.getUserList();
            vm.btnDisabled = undefined;
        });
    };

    vm.getUserList = function(){
        userSrvc.getUserList().then(function(response){
            vm.list = response.data.data;
        });
    };

    vm.openUserDetails = function(index){
        vm.index = index;
        $uibModal.open({
            templateUrl: '/app/modules/user/partials/view-user.html',
            controller: showUserDetail,
            scope: $scope,
            size: 'lg'
        });
    };

    function showUserDetail($scope, $uibModalInstance) {
        vm.userDetail = vm.list[vm.index];
        vm.modalHeader = "User Details";
        if(vm.userDetail.address.state){
            vm.updateCityList(vm.userDetail.address.state);
        }
        vm.ok = function () {
            userSrvc.modifyUser(vm.userDetail).then(function(response){
                toastr.success('Updated Successfully');
                vm.getUserList();
                $uibModalInstance.close();
            });
        };
        
        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    vm.deleteUser = function(index){
        var id = {"userId": vm.list[index]._id};
        userSrvc.deleteUser(id).then(function(response){
            toastr.success('Deleted Successfully');
            vm.getUserList();
        });
    };
}
var userApp = angular.module('bips.userApp', []);
userApp.controller('userCtrl', userCtrl);

function admissionCtrl($scope, globalConst, commonSrvc, admissionSrvc, toastr, $state, $rootScope){
    var vm = this;
    vm.name = "Student Details";
    vm.studentDetails = {};
    vm.motherTongue = [];
    vm.id = [];
    vm.cities = [];
    vm.active = 0;
    vm.disabled = 1;
    $rootScope.leftNavBar = true;
    $rootScope.topNavBar = true;
    vm.tabs = [
        { title:'Student Details', content:'modules/admission/student.html' },
        { title:'Parent Details', content:'modules/admission/parent.html' },
        { title:'Class Details', content:'modules/admission/class.html' },
        { title:'Preview', content:'modules/admission/payment.html' },
    ];
    vm.studentDetails.date = globalConst.date;
    vm.format = globalConst.dateFormat;
    vm.dateOptions = globalConst.datePickerOpt;
    vm.open = function() {
        vm.popup.opened = true;
    };
    vm.popup = {
        opened: false
    };
    vm.bloodGroup = globalConst.bloodGroup;
    vm.motherTongue = globalConst.motherTongue;
    vm.id = globalConst.id;
    vm.idList = globalConst.idList;
    var cityState = globalConst.cityState;
    vm.states = Object.keys(cityState);
    vm.updateCityList = function(state){
        vm.cities = cityState[state];
    };
    vm.occupations = globalConst.occupations;
    vm.class = globalConst.class;
    vm.getClassDetails = function(data){
        vm.classDetails = data;
        console.log(vm.classDetails, vm.parentDetails, vm.studentDetails);
    };
    vm.moveForward = function(data, tabIndex){
        if(tabIndex === 1){
            vm.student.dob = Date.parse(data.student.dob);
        }
        vm.active = tabIndex;
        if(tabIndex === 3){
            vm.disabled = 0;
            vm.student.classDetails = data;
            getRollNumber(vm.student.classDetails.name);
        }
    };
    vm.save = function(data ){
        admissionSrvc.saveStudent(data).then(function(response){
            toastr.success('Saved Successfully');
            $state.go('student');
        });
    };
    vm.sameAsPresentAdd = function(data, checked, parent){
        if(checked){
            if(parent === "mother"){
                vm.student.parentDetails.motherDetails.address.permanent = data;
            }
            if(parent === "father"){
                vm.student.parentDetails.fatherDetails.address.permanent = data;
            }
        }
        else{
            if(parent === "mother"){
                vm.student.parentDetails.motherDetails.address.permanent = {};
            }
            if(parent === "father"){
                vm.student.parentDetails.fatherDetails.address.permanent = {};
            }
        }
    };
    function getRollNumber(className){
        admissionSrvc.getRollNumber({"className": className}).then(function(response){
            vm.student.rollNumber = response.data.data;
        });
    }
}
var admissionApp = angular.module('bips.admissionApp', []);
admissionApp.controller('admissionCtrl', admissionCtrl);
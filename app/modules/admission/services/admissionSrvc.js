function admissionSrvc(commonSrvc, $sessionStorage){
    var factory = {};
    factory.saveStudent = function(data){
        return commonSrvc.post('student/api/v1/admission', data, $sessionStorage.get('auth')).then(function(response){
            return response;
        });
    };

    factory.getRollNumber = function(data){
        return commonSrvc.post('student/api/v1/get/roll', data, $sessionStorage.get('auth')).then(function(response){
            return response;
        });
    };
    
    return factory;
}
admissionApp.factory('admissionSrvc', admissionSrvc);
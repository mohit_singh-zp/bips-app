function facultySrvc(commonSrvc, $sessionStorage){
    var factory = {};
    factory.getFacultyList = function(){
        return commonSrvc.get('faculty/api/v1/get/list', '', $sessionStorage.get('auth')).then(function(response){
            return response;
        });
    };

    factory.saveFaculty = function(data){
        return commonSrvc.post('faculty/api/v1/add', data, $sessionStorage.get('auth')).then(function(response){
            return response;
        });
    };

    factory.updateFacultyList = function(data){
        return commonSrvc.update('faculty/api/v1/update', data, $sessionStorage.get('auth')).then(function(response){
            return response;
        });
    };

    factory.getAssignList = function(){
        return commonSrvc.get('faculty/api/v1/assigned/student/list', '', $sessionStorage.get('auth')).then(function(response){
            return response;
        });
    };

    factory.saveAssignedList = function(data){
        return commonSrvc.update('faculty/api/v1/assign/student', data, $sessionStorage.get('auth')).then(function(response){
            return response;
        });
    };

    factory.deleteFaculty = function(data){
        return commonSrvc.update('faculty/api/v1/delete', data, $sessionStorage.get('auth')).then(function(response){
            return response;
        });
    };
    return factory;
}
facultyApp.factory('facultySrvc', facultySrvc);
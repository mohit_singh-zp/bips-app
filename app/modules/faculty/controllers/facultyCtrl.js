function facultyCtrl($scope, globalConst, facultySrvc, commonSrvc, $uibModal, toastr, $rootScope){
    var vm = this;
    vm.AssignedRecords = [];
    vm.teacher = {};
    vm.active = 0;
    vm.tabs = [
        { id: 0, title:'view', content:'modules/faculty/faculty-view.html' },
        { id: 1, title:'create', content:'modules/faculty/faculty-admission.html' }
    ];
    $rootScope.leftNavBar = true;
    $rootScope.topNavBar = true;
    vm.idList = globalConst.idList;
    vm.motherTongue = globalConst.motherTongue;
    vm.maritalStatus = globalConst.maritalStatus;
    var cityState = globalConst.cityState;
    vm.states = Object.keys(cityState);
    vm.updateCityList = function(state, type){
        if(type == 'permanent'){
            vm.permanentCities = cityState[state];
        }else{
            vm.presentCities = cityState[state];
        }
    };

    vm.manageTab = function(tabIndex){
        if(!vm.list.length){
            vm.active = 1;
        }
    };

    vm.sameAsPresentAdd = function(data, checked){
        if(checked){
            vm.details.address.permanent = data;
        }
        else{
            vm.details.address.permanent = {};
        }
    };

    vm.saveFaculty = function(data, form){
        vm.btnDisabled = facultySrvc.saveFaculty(data).then(function(response){
            toastr.success('Saved Successfully.');
            vm.btnDisabled = undefined;
            vm.active = 0;
            vm.getFacultyList();
        });
    };
    vm.getFacultyList = function(){
        facultySrvc.getFacultyList().then(function(response){
            vm.list = response.data.data;
            if(!vm.list.length){
                vm.active = 1;
            }
        });
    };
    vm.openFacultyDetails = function(index){
        vm.primaryDetails = angular.copy(vm.list[index]);
        vm.index = index;
        $uibModal.open({
            templateUrl: 'modules/faculty/partials/view-faculty-details.html',
            controller: showFacultyDetail,
            scope: $scope,
            size: 'lg',
        });
    };
    function showFacultyDetail($scope, $uibModalInstance, globalConst) {
        vm.details = vm.list[vm.index];
        if(vm.details.address.permanent.state){
            vm.updateCityList(vm.details.address.permanent.state, 'permanent');
        }
        if(vm.details.address.present.state){
            vm.updateCityList(vm.details.address.present.state, 'present');
        }
        vm.ok = function () {
            facultySrvc.updateFacultyList(vm.details).then(function(response){
                toastr.success('Updated Successfully');
                vm.getFacultyList();
                $uibModalInstance.close();
            });
        };
        
        vm.cancel = function () {
            vm.details = vm.primaryDetails;
            vm.list[vm.index] = vm.primaryDetails;
            $uibModalInstance.dismiss('cancel');
        };
    }

    vm.openAsignList = function(data, index){
        vm.assignedStudent = data;
        vm.modalHeader = "Assign List";
        vm.assignFlag = false;
        vm.facultyId = vm.list[index]._id;
        getAssignList(data, index);
        
        $uibModal.open({
            templateUrl: 'modules/faculty/partials/student-assign-list.html',
            controller: showAsignList,
            scope: $scope,
            size: 'lg',
        });
    };

    function showAsignList($scope, $uibModalInstance){
        vm.ok = function (flag) {
            var assignedStudentList = {student: []};
            assignedStudentList.facultyId = vm.facultyId;
            vm.AssignedRecords.forEach(function(element) {
                assignedStudentList.student.push(vm.assignList[element]);
            }, this);
            facultySrvc.saveAssignedList(assignedStudentList).then(function(response){
                toastr.success('Successfully Assigned');
                vm.getFacultyList();
                $uibModalInstance.close();
            });
        };
        
        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    function getAssignList(data, index){
        var assignedStudent = [];
        facultySrvc.getAssignList().then(function(response){
            vm.primaryAssignList = angular.copy(response.data.data);
            if(data){
                data.forEach(function(element){
                    element.assigned = true;
                    response.data.data.push(element);
                });
                vm.assignList = response.data.data;
            }else{
                vm.assignList = response.data.data;
            }
            for(var i=vm.primaryAssignList.length; i<=vm.primaryAssignList.length+vm.assignedStudent.length-1; i++){
                vm.AssignedRecords.push(i);
            }
        });
    }

    vm.currentRecord = function(record, selected){
        selected = selected == undefined ? true : selected; 
        if(selected){
            vm.AssignedRecords.push(record);
        }else{
            vm.AssignedRecords.splice(vm.AssignedRecords.indexOf(record), 1);
        }
        
    };

    vm.deleteFaculty = function(index){
        facultySrvc.deleteFaculty({'facultyId':vm.list[index]._id}).then(function(response){
            toastr.success('Deleted Successfully');
            vm.getFacultyList();
        });
    };
}
var facultyApp = angular.module('bips.facultyApp', []);
facultyApp.controller('facultyCtrl', facultyCtrl);
app.factory('commonSrvc', function($http, $sessionStorage){
    var intercept = {};
    intercept.interceptor = function($q, $rootScope){
        return {
            'request': function (config) {
                if(config.url === "modules/login/login.html"){
                    $rootScope.leftNavBar = false;
                    $rootScope.topNavBar = false;
                }else{
                    $rootScope.leftNavBar = true;
                    $rootScope.topNavBar = true;
                }
                return config || $q.when(config);
            },
            'response': function (response) {
                return response || $q.when(response);
            },
            'responseError': function (rejection) {
                return $q.reject(rejection);
            }
        };
    };
    intercept.apiUrl = function(){
        return "http://139.59.80.68:3000/";
        // return "http://localhost:3000/";
    };
    intercept.setAuth = function(auth, role){
        $sessionStorage.put("auth", auth);
        $sessionStorage.put("role", role);
    };
    intercept.getAuth = function(){
        var auth = $sessionStorage.get('auth') || '';
        return auth;
    };
    intercept.getRole = function(){
        var role = $sessionStorage.get('role') || undefined;
        return role;
    };
    intercept.post = function(url, request, header){
        if(header){
            return $http({
                url: intercept.apiUrl()+url,
                method: "POST",
                data: request,
                headers: {'Content-Type':'application/json', 'Authorization':intercept.getAuth()}
            })
            .then(function(response) {
                return response;
            });
        }else{
            return $http({
                url: intercept.apiUrl()+url,
                method: "POST",
                data: request,
            })
            .then(function(response) {
                return response;
            });
        }
    };
    intercept.get = function(url, request, header){
        if(request !== ""){
            return $http({
                url: intercept.apiUrl()+url,
                method: "GET",
                headers: {'Content-Type':'application/json', 'Authorization':intercept.getAuth()}
            })
            .then(function(response) {
                return response;
            });
        }else{
            return $http({
                url: intercept.apiUrl()+url,
                method: "GET",
                headers: {'Content-Type':'application/json', 'Authorization':intercept.getAuth()},
                data: request
            })
            .then(function(response) {
                return response;
            });
        }
        
    };
    intercept.update = function(url, request, header){
        return $http({
            url: intercept.apiUrl()+url,
            method: "PUT",
            headers: {'Content-Type':'application/json', 'Authorization':intercept.getAuth()},
            data: request
        })
        .then(function(response) {
            return response;
        });
    };
    intercept.delete = function(url, request, header){
        return $http({
            url: intercept.apiUrl()+url,
            method: "PUT",
            headers: {'Content-Type':'application/json', 'Authorization':intercept.getAuth()},
            data: request
        })
        .then(function(response) {
            return response;
        });
    };
    return intercept;
});
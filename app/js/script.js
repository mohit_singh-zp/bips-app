var app = angular.module('bips', 
[
    'ngAnimate',
    'ngSanitize',
    'ui.bootstrap',
    'ui.router',
    'swxSessionStorage',
    'toastr',
    'constants',
    'bips.loginApp',
    'bips.admissionApp',
    'bips.facultyApp',
    'bips.noticeApp',
    'bips.userApp',
    'bips.studentApp',
    'bips.dashboardApp'
]);

function inter($q, $rootScope, $injector){
    return {
        'request': function (config) {
            $rootScope.showLoader = true;
            return config || $q.when(config);
        },
        'response': function (response) {
            $rootScope.showLoader = false;
            return response || $q.when(response);
        },
        'responseError': function (rejection) {
            if(rejection.data.message === "Authentication Failed" || rejection.data.message === "Wrong Email id" || rejection.data.message === "Wrong password"){
                $injector.get('$state').transitionTo('login');
                $injector.get('toastr').clear();
                $injector.get('toastr').error(rejection.data.message || rejection.data.message.message, 'Login Error');
            }else if(rejection.data.message.name){
                $injector.get('$state').transitionTo('login');
                $injector.get('toastr').clear();
                $injector.get('toastr').error('invalid token', 'Login Error');
            }else{
                $injector.get('toastr').clear();
                $injector.get('toastr').error(rejection.data.message || rejection.data.message.message, 'Error');
            }
            $rootScope.showLoader = false;
            return $q.reject(rejection);
        }
    };
}

app.config(function($stateProvider, $urlRouterProvider, $httpProvider, commonSrvcProvider, $qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
    $httpProvider.interceptors.push(inter);

    $urlRouterProvider.otherwise('/login');
    
    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'modules/login/login.html',
            controller: 'loginCtrl',
            controllerAs: 'login'
        })
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: 'modules/dashboard/dashboard.html',
            controller: 'dashboardCtrl',
            controllerAs: 'dashboard'
        })
        .state('user', {
            url: '/user',
            templateUrl: 'modules/user/user.html',
            controller: 'userCtrl',
            controllerAs: 'user'
        })
        .state('user.create', {
            url: '/create',
            templateUrl: 'modules/user/user-create.html',
            controller: 'userCtrl',
            controllerAs: 'user'
        })
        .state('user.view', {
            url: '/view',
            templateUrl: 'modules/user/user-view.html',
            controller: 'userCtrl',
            controllerAs: 'user'
        })
        .state('student', {
            url: '/student',
            templateUrl: 'modules/student/student.html',
            controller: 'studentCtrl',
            controllerAs: 'student'
        })
		.state('admission', {
            url: '/admission',
            templateUrl: 'modules/admission/admission.html',
            controller: 'admissionCtrl',
            controllerAs: 'admission'
        })
        .state('faculty', {
            url: '/faculty',
            templateUrl: 'modules/faculty/faculty.html',
            controller: 'facultyCtrl',
            controllerAs: 'faculty'
        })
		.state('faculty.admission', {
            url: '/admission',
            templateUrl: 'modules/faculty/faculty-admission.html',
            controller: 'facultyCtrl',
            controllerAs: 'faculty'
        })
        .state('faculty.view', {
            url: '/view',
            templateUrl: 'modules/faculty/faculty-view.html',
            controller: 'facultyCtrl',
            controllerAs: 'faculty'
        })
		.state('notice', {
            url: '/notice',
            templateUrl: 'modules/notice-board/notice.html',
            controller: 'noticeCtrl',
            controllerAs: 'notice'
        })
});